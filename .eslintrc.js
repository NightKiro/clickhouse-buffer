module.exports = {
  "env": {
    "browser": true,
    "es6": true
  },
  "extends": ["eslint:recommended", "standard"],
  "rules": {
    "indent": [
      "error",
      2
    ],
    "linebreak-style": [
      "error",
      "unix"
    ],
    "object-curly-spacing": [
        "error",
      "always"
    ],
    "quotes": [
      "error",
      "single",
      "avoid-escape"
    ],
    "semi": [
      "error",
      "never"
    ],
    "prefer-const": ["error"],
    "strict": [
      "error",
      "global"
    ]
  }
};
