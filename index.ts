const hapi = require('@hapi/hapi')

async function start () {
  const server = await hapi.server({
    port: process.env.PORT || 3333,
    host: 'localhost',
    routes: {
      validate: {
        failAction: async (_request: any, _h: any, err: { isBoom: any }) => {
          if (err.isBoom) {
            console.error(err)
            throw err
          }
        }
      }
    }
  })

  await server.route(require('./app/controllers/buffer'))

  await server.start()

  console.log(`Server running on ${server.info.uri}`)
}

start()
