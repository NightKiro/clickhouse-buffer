namespace NodeJS {
    interface ProcessEnv {
        REDIS_URL: string
        CLICKERHOUSE_URL: string
        CLICKERHOUSE_PASSWORD: string
        MAX_BUFFER_SIZE: number
        BUFFER_LIFETIME: number
    }
  }