const redis = require('ioredis')
const dotenv = require('dotenv');
const { ClickHouseClient } = require('@depyronick/clickhouse-client');
dotenv.config()

const CLICKERHOUSE_URL: any = process.env.CLICKERHOUSE_URL
const CLICKERHOUSE_PASSWORD: any = process.env.CLICKERHOUSE_PASSWORD

const MAX_SIZE: any = process.env.MAX_BUFFER_SIZE
const BUFFER_LIFETIME: any = process.env.BUFFER_LIFETIME

const client = redis.createClient(process.env.REDIS_URL)
const databaseServer = new ClickHouseClient({
  host: CLICKERHOUSE_URL,
  password: CLICKERHOUSE_PASSWORD
})

module.exports = class BufferService {
  static async addRequestToBuffer (data: JSON, tableName: String) {
    const flag = await client.setnx(`flag${tableName}`, tableName)

    // setnx will return 1 if key not existed and we will start a timer to send buffer by lifecycle
    if(flag === 1) {
      this.sentBufferByLifetime(tableName)
    }

    const entryJSON = JSON.stringify(data);
    await client.lpush(tableName, entryJSON);
    const listLength = await client.llen(tableName)

    if(listLength >= MAX_SIZE) {
      const buffer = await client.lrange(tableName, 0, MAX_SIZE-1);

      this.sendBuffer(buffer, tableName)
      for(let i = 0; i < MAX_SIZE; i++) {
          await client.lpop(tableName);
      }

        // удаляем флаг, чтобы таймер не отправил пакет данных
      await client.del(`flag${tableName}`)
    }

    return { success: true }
  }

  static async sentBufferByLifetime (tableName: String) {
    await new Promise(f => setTimeout(f, BUFFER_LIFETIME))

    const hasBufferInRedis = await client.get(`flag${tableName}`)

    // Проверка что буффер обмена ещё не был отправлен и последующая отправка по истечению срока хранения
    if(hasBufferInRedis) {
      const listLength = client.llen(tableName)

      const buffer = await client.lrange(tableName, 0, listLength);

      this.sendBuffer(buffer, tableName)
      for(let i = 0; i < listLength; i++) {
          await client.lpop(tableName);
      }

      await client.del(`flag${tableName}`)
    }
  }

  static async sendBuffer (data: any, tableName: String) {
    databaseServer.insert(tableName, data.map((s: string) => JSON.parse(s)))
  .subscribe({
    error: (err: any) => {
      console.log(err)
    },
    next: (row: any) => {
      console.log(row)

    }
  })
  }
}
