const BufferService = require('../service/buffer')

class BufferController {
  /**
   * Put's json from request to a buffer
   */
  static async addRequestToBuffer (req: { payload: JSON; params: { tableName: String } }, h: any) {
     const response = await BufferService.addRequestToBuffer(req.payload, req.params.tableName)
     
     return response
  }
}

module.exports = [
  {
    method: 'POST',
    path: '/v1/clickhouse/insert/{tableName}',
    handler: BufferController.addRequestToBuffer,
  }
]
