# clickhouse-buffer
## dependencies
- Node.JS 12.XX
- TypeScript 4.8.X
- ClickerHouse

## Getting Started
После установки всех необходимых компонентов, скомпилируйте тайпскрипт

Установите значения переменных окружения в .env, пример .env.example


Для удобства разработки используйте watcher mode
```bash
npm run dev:tsc -w
```

Для запуска проекта используйте
```bash
npm run dev:start
```